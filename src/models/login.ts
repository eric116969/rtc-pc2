import { stringify } from 'querystring';
import { history, Reducer, Effect } from 'umi';
import { getPageQuery } from '@/utils/utils';

export interface StateType {
  status?: 'ok' | 'error';
  type?: string;
  currentAuthority?: 'user' | 'guest' | 'admin';
}

export interface ModelType {
  namespace: string;
  state: StateType;
  effects: {
    logout: Effect;
  };
}

const Model: ModelType = {
  namespace: 'login',

  state: {
    status: undefined
  },

  effects: {
    *logout() {
      const { redirect } = getPageQuery();
      // Note: There may be security issues, please note
      if (window.location.pathname !== '/user/login' && !redirect) {
        history.replace({
          pathname: '/user/login',
          search: stringify({
            redirect: window.location.href
          })
        });
      }
    }
  }
};

export default Model;
