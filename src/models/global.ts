import { Effect } from 'dva';
import { Reducer, connect } from 'umi';

const GlobalModel = {
  namespace: 'global',
  state: {
    collapsed: false,
    notices: []
  },
  effects: {},
  reducers: {},
  subscription: {}
};

export default GlobalModel;
