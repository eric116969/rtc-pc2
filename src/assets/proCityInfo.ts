const proCityInfo = [
  {
    label: '山东',
    children: [
      { label: '济南', value: '276', children: [] },
      { label: '青岛', value: '277', children: [] },
      {
        label: '威海',
        value: '278',
        children: []
      },
      { label: '烟台', value: '279', children: [] },
      { label: '潍坊', value: '280', children: [] },
      {
        label: '泰安',
        value: '281',
        children: []
      },
      { label: '滨州', value: '282', children: [] },
      { label: '德州', value: '283', children: [] },
      {
        label: '东营',
        value: '284',
        children: []
      },
      { label: '菏泽', value: '285', children: [] },
      { label: '济宁', value: '286', children: [] },
      {
        label: '聊城',
        value: '287',
        children: []
      },
      { label: '临沂', value: '288', children: [] },
      { label: '莱芜', value: '289', children: [] },
      {
        label: '日照',
        value: '290',
        children: []
      },
      { label: '淄博', value: '291', children: [] },
      { label: '枣庄', value: '292', children: [] }
    ],
    value: -1
  },
  {
    label: '福建',
    children: [
      { label: '福州', value: '28', children: [] },
      { label: '厦门', value: '29', children: [] },
      {
        label: '泉州',
        value: '30',
        children: []
      },
      { label: '龙岩', value: '31', children: [] },
      { label: '宁德', value: '32', children: [] },
      {
        label: '南平',
        value: '33',
        children: []
      },
      { label: '莆田', value: '34', children: [] },
      { label: '三明', value: '35', children: [] },
      {
        label: '漳州',
        value: '36',
        children: []
      }
    ],
    value: -2
  },
  {
    label: '台湾',
    children: [{ label: '台湾', value: '396', children: [] }],
    value: -3
  },
  {
    label: '河北',
    children: [
      { label: '石家庄', value: '119', children: [] },
      { label: '保定', value: '120', children: [] },
      {
        label: '承德',
        value: '121',
        children: []
      },
      { label: '邯郸', value: '122', children: [] },
      { label: '唐山', value: '123', children: [] },
      {
        label: '秦皇岛',
        value: '124',
        children: []
      },
      { label: '沧州', value: '125', children: [] },
      { label: '衡水', value: '126', children: [] },
      {
        label: '廊坊',
        value: '127',
        children: []
      },
      { label: '邢台', value: '128', children: [] },
      { label: '张家口', value: '129', children: [] }
    ],
    value: -4
  },
  {
    label: '河南',
    children: [
      { label: '郑州', value: '131', children: [] },
      { label: '洛阳', value: '132', children: [] },
      {
        label: '开封',
        value: '133',
        children: []
      },
      { label: '许昌', value: '134', children: [] },
      { label: '安阳', value: '135', children: [] },
      {
        label: '平顶山',
        value: '136',
        children: []
      },
      { label: '鹤壁', value: '137', children: [] },
      { label: '焦作', value: '138', children: [] },
      {
        label: '济源',
        value: '139',
        children: []
      },
      { label: '漯河', value: '140', children: [] },
      { label: '南阳', value: '141', children: [] },
      {
        label: '濮阳',
        value: '142',
        children: []
      },
      { label: '三门峡', value: '143', children: [] },
      { label: '商丘', value: '144', children: [] },
      {
        label: '新乡',
        value: '145',
        children: []
      },
      { label: '信阳', value: '146', children: [] },
      { label: '驻马店', value: '147', children: [] },
      {
        label: '周口',
        value: '148',
        children: []
      }
    ],
    value: -5
  },
  {
    label: '重庆',
    children: [{ label: '重庆', value: '8', children: [] }],
    value: -6
  },
  {
    label: '湖北',
    children: [
      { label: '武汉', value: '164', children: [] },
      { label: '天门', value: '165', children: [] },
      {
        label: '十堰',
        value: '166',
        children: []
      },
      { label: '黄石', value: '167', children: [] },
      { label: '鄂州', value: '168', children: [] },
      {
        label: '恩施',
        value: '169',
        children: []
      },
      { label: '黄冈', value: '170', children: [] },
      { label: '荆州', value: '171', children: [] },
      {
        label: '荆门',
        value: '172',
        children: []
      },
      { label: '随州', value: '173', children: [] },
      { label: '宜昌', value: '174', children: [] },
      {
        label: '襄阳',
        value: '175',
        children: []
      },
      { label: '潜江', value: '176', children: [] },
      { label: '仙桃', value: '177', children: [] },
      {
        label: '孝感',
        value: '178',
        children: []
      },
      { label: '咸宁', value: '179', children: [] },
      { label: '神农架', value: '180', children: [] }
    ],
    value: -7
  },
  {
    label: '湖南',
    children: [
      { label: '长沙', value: '182', children: [] },
      { label: '岳阳', value: '183', children: [] },
      {
        label: '湘潭',
        value: '184',
        children: []
      },
      { label: '常德', value: '185', children: [] },
      { label: '郴州', value: '186', children: [] },
      {
        label: '衡阳',
        value: '187',
        children: []
      },
      { label: '怀化', value: '188', children: [] },
      { label: '娄底', value: '189', children: [] },
      {
        label: '邵阳',
        value: '190',
        children: []
      },
      { label: '益阳', value: '191', children: [] },
      { label: '永州', value: '192', children: [] },
      {
        label: '株洲',
        value: '193',
        children: []
      },
      { label: '张家界', value: '194', children: [] },
      { label: '湘西', value: '195', children: [] }
    ],
    value: -8
  },
  {
    label: '海南',
    children: [
      { label: '海口', value: '100', children: [] },
      { label: '三亚', value: '101', children: [] },
      {
        label: '白沙县',
        value: '102',
        children: []
      },
      { label: '保亭县', value: '103', children: [] },
      { label: '昌江县', value: '104', children: [] },
      {
        label: '澄迈县',
        value: '105',
        children: []
      },
      { label: '定安县', value: '106', children: [] },
      { label: '东方', value: '107', children: [] },
      {
        label: '乐东县',
        value: '108',
        children: []
      },
      { label: '临高县', value: '109', children: [] },
      { label: '陵水县', value: '110', children: [] },
      {
        label: '琼海',
        value: '111',
        children: []
      },
      { label: '琼中县', value: '112', children: [] },
      { label: '屯昌县', value: '113', children: [] },
      {
        label: '万宁',
        value: '114',
        children: []
      },
      { label: '文昌', value: '115', children: [] },
      { label: '五指山', value: '116', children: [] },
      {
        label: '儋州',
        value: '117',
        children: []
      }
    ],
    value: -9
  },
  {
    label: '江西',
    children: [
      { label: '南昌', value: '221', children: [] },
      { label: '赣州', value: '222', children: [] },
      {
        label: '九江',
        value: '223',
        children: []
      },
      { label: '景德镇', value: '224', children: [] },
      { label: '吉安', value: '225', children: [] },
      {
        label: '萍乡',
        value: '226',
        children: []
      },
      { label: '上饶', value: '227', children: [] },
      { label: '新余', value: '228', children: [] },
      {
        label: '宜春',
        value: '229',
        children: []
      },
      { label: '鹰潭', value: '230', children: [] },
      { label: '抚州', value: '231', children: [] }
    ],
    value: -10
  },
  {
    label: '黑龙江',
    children: [
      { label: '哈尔滨', value: '150', children: [] },
      { label: '大庆', value: '151', children: [] },
      {
        label: '齐齐哈尔',
        value: '152',
        children: []
      },
      { label: '佳木斯', value: '153', children: [] },
      { label: '大兴安岭', value: '154', children: [] },
      {
        label: '黑河',
        value: '155',
        children: []
      },
      { label: '鹤岗', value: '156', children: [] },
      { label: '鸡西', value: '157', children: [] },
      {
        label: '牡丹江',
        value: '158',
        children: []
      },
      { label: '七台河', value: '159', children: [] },
      { label: '绥化', value: '160', children: [] },
      {
        label: '双鸭山',
        value: '161',
        children: []
      },
      { label: '伊春', value: '162', children: [] }
    ],
    value: -11
  },
  {
    label: '天津',
    children: [{ label: '天津', value: '6', children: [] }],
    value: -12
  },
  {
    label: '贵州',
    children: [
      { label: '贵阳', value: '90', children: [] },
      { label: '安顺', value: '91', children: [] },
      {
        label: '遵义',
        value: '92',
        children: []
      },
      { label: '六盘水', value: '93', children: [] },
      { label: '毕节', value: '94', children: [] },
      {
        label: '黔东南',
        value: '95',
        children: []
      },
      { label: '黔西南', value: '96', children: [] },
      { label: '黔南', value: '97', children: [] },
      {
        label: '铜仁',
        value: '98',
        children: []
      }
    ],
    value: -13
  },
  {
    label: '陕西',
    children: [
      { label: '西安', value: '306', children: [] },
      { label: '安康', value: '307', children: [] },
      {
        label: '宝鸡',
        value: '308',
        children: []
      },
      { label: '汉中', value: '309', children: [] },
      { label: '商洛', value: '310', children: [] },
      {
        label: '铜川',
        value: '311',
        children: []
      },
      { label: '渭南', value: '312', children: [] },
      { label: '咸阳', value: '313', children: [] },
      {
        label: '延安',
        value: '314',
        children: []
      },
      { label: '榆林', value: '315', children: [] }
    ],
    value: -14
  },
  {
    label: '新疆',
    children: [
      { label: '乌鲁木齐', value: '347', children: [] },
      { label: '石河子', value: '348', children: [] },
      {
        label: '吐鲁番',
        value: '349',
        children: []
      },
      { label: '伊犁', value: '350', children: [] },
      { label: '阿克苏', value: '351', children: [] },
      {
        label: '阿勒泰',
        value: '352',
        children: []
      },
      { label: '巴音', value: '353', children: [] },
      { label: '博尔塔拉', value: '354', children: [] },
      {
        label: '昌吉',
        value: '355',
        children: []
      },
      { label: '哈密', value: '356', children: [] },
      { label: '和田', value: '357', children: [] },
      {
        label: '喀什',
        value: '358',
        children: []
      },
      { label: '克拉玛依', value: '359', children: [] },
      { label: '克孜勒', value: '360', children: [] },
      {
        label: '塔城',
        value: '361',
        children: []
      }
    ],
    value: -15
  },
  {
    label: '澳门',
    children: [{ label: '澳门', value: '394', children: [] }],
    value: -16
  },
  {
    label: '江苏',
    children: [
      { label: '南京', value: '207', children: [] },
      { label: '苏州', value: '208', children: [] },
      {
        label: '常州',
        value: '209',
        children: []
      },
      { label: '连云港', value: '210', children: [] },
      { label: '泰州', value: '211', children: [] },
      {
        label: '无锡',
        value: '212',
        children: []
      },
      { label: '徐州', value: '213', children: [] },
      { label: '扬州', value: '214', children: [] },
      {
        label: '镇江',
        value: '215',
        children: []
      },
      { label: '淮安', value: '216', children: [] },
      { label: '南通', value: '217', children: [] },
      {
        label: '宿迁',
        value: '218',
        children: []
      },
      { label: '盐城', value: '219', children: [] }
    ],
    value: -17
  },
  {
    label: '安徽',
    children: [
      { label: '合肥', value: '10', children: [] },
      { label: '芜湖', value: '11', children: [] },
      {
        label: '安庆',
        value: '12',
        children: []
      },
      { label: '蚌埠', value: '13', children: [] },
      { label: '亳州', value: '14', children: [] },
      {
        label: '巢湖',
        value: '15',
        children: []
      },
      { label: '池州', value: '16', children: [] },
      { label: '滁州', value: '17', children: [] },
      {
        label: '阜阳',
        value: '18',
        children: []
      },
      { label: '黄山', value: '19', children: [] },
      { label: '淮北', value: '20', children: [] },
      {
        label: '淮南',
        value: '21',
        children: []
      },
      { label: '六安', value: '22', children: [] },
      { label: '马鞍山', value: '23', children: [] },
      {
        label: '宿州',
        value: '24',
        children: []
      },
      { label: '铜陵', value: '25', children: [] },
      { label: '宣城', value: '26', children: [] }
    ],
    value: -18
  },
  {
    label: '西藏',
    children: [
      { label: '拉萨', value: '339', children: [] },
      { label: '日喀则', value: '340', children: [] },
      {
        label: '阿里',
        value: '341',
        children: []
      },
      { label: '昌都', value: '342', children: [] },
      { label: '林芝', value: '343', children: [] },
      {
        label: '那曲',
        value: '344',
        children: []
      },
      { label: '山南', value: '345', children: [] }
    ],
    value: -19
  },
  {
    label: '上海',
    children: [{ label: '上海', value: '4', children: [] }],
    value: -20
  },
  {
    label: '吉林',
    children: [
      { label: '长春', value: '197', children: [] },
      { label: '吉林', value: '198', children: [] },
      {
        label: '延边',
        value: '199',
        children: []
      },
      { label: '白城', value: '200', children: [] },
      { label: '白山', value: '201', children: [] },
      {
        label: '辽源',
        value: '202',
        children: []
      },
      { label: '四平', value: '203', children: [] },
      { label: '松原', value: '204', children: [] },
      {
        label: '通化',
        value: '205',
        children: []
      }
    ],
    value: -21
  },
  {
    label: '山西',
    children: [
      { label: '太原', value: '294', children: [] },
      { label: '长治', value: '295', children: [] },
      {
        label: '大同',
        value: '296',
        children: []
      },
      { label: '晋城', value: '297', children: [] },
      { label: '晋中', value: '298', children: [] },
      {
        label: '临汾',
        value: '299',
        children: []
      },
      { label: '吕梁', value: '300', children: [] },
      { label: '朔州', value: '301', children: [] },
      {
        label: '忻州',
        value: '302',
        children: []
      },
      { label: '运城', value: '303', children: [] },
      { label: '阳泉', value: '304', children: [] }
    ],
    value: -22
  },
  {
    label: '甘肃',
    children: [
      { label: '兰州', value: '38', children: [] },
      { label: '白银', value: '39', children: [] },
      {
        label: '定西',
        value: '40',
        children: []
      },
      { label: '金昌', value: '41', children: [] },
      { label: '酒泉', value: '42', children: [] },
      {
        label: '平凉',
        value: '43',
        children: []
      },
      { label: '庆阳', value: '44', children: [] },
      { label: '武威', value: '45', children: [] },
      {
        label: '天水',
        value: '46',
        children: []
      },
      { label: '张掖', value: '47', children: [] },
      { label: '甘南', value: '48', children: [] },
      {
        label: '嘉峪关',
        value: '49',
        children: []
      },
      { label: '临夏', value: '50', children: [] },
      { label: '陇南', value: '51', children: [] }
    ],
    value: -23
  },
  {
    label: '宁夏',
    children: [
      { label: '银川', value: '261', children: [] },
      { label: '石嘴山', value: '262', children: [] },
      {
        label: '固原',
        value: '263',
        children: []
      },
      { label: '吴忠', value: '264', children: [] },
      { label: '中卫', value: '265', children: [] }
    ],
    value: -24
  },
  {
    label: '香港',
    children: [{ label: '香港', value: '392', children: [] }],
    value: -25
  },
  {
    label: '四川',
    children: [
      { label: '成都', value: '317', children: [] },
      { label: '绵阳', value: '318', children: [] },
      {
        label: '资阳',
        value: '319',
        children: []
      },
      { label: '巴中', value: '320', children: [] },
      { label: '德阳', value: '321', children: [] },
      {
        label: '达州',
        value: '322',
        children: []
      },
      { label: '广安', value: '323', children: [] },
      { label: '广元', value: '324', children: [] },
      {
        label: '乐山',
        value: '325',
        children: []
      },
      { label: '泸州', value: '326', children: [] },
      { label: '眉山', value: '327', children: [] },
      {
        label: '内江',
        value: '328',
        children: []
      },
      { label: '南充', value: '329', children: [] },
      { label: '攀枝花', value: '330', children: [] },
      {
        label: '遂宁',
        value: '331',
        children: []
      },
      { label: '宜宾', value: '332', children: [] },
      { label: '雅安', value: '333', children: [] },
      {
        label: '自贡',
        value: '334',
        children: []
      },
      { label: '阿坝', value: '335', children: [] },
      { label: '甘孜', value: '336', children: [] },
      {
        label: '凉山',
        value: '337',
        children: []
      }
    ],
    value: -26
  },
  {
    label: '浙江',
    children: [
      { label: '杭州', value: '380', children: [] },
      { label: '宁波', value: '381', children: [] },
      {
        label: '嘉兴',
        value: '382',
        children: []
      },
      { label: '绍兴', value: '383', children: [] },
      { label: '温州', value: '384', children: [] },
      {
        label: '舟山',
        value: '385',
        children: []
      },
      { label: '湖州', value: '386', children: [] },
      { label: '金华', value: '387', children: [] },
      {
        label: '丽水',
        value: '388',
        children: []
      },
      { label: '台州', value: '389', children: [] },
      { label: '衢州', value: '390', children: [] }
    ],
    value: -27
  },
  {
    label: '广西',
    children: [
      { label: '南宁', value: '75', children: [] },
      { label: '北海', value: '76', children: [] },
      {
        label: '防城港',
        value: '77',
        children: []
      },
      { label: '桂林', value: '78', children: [] },
      { label: '柳州', value: '79', children: [] },
      {
        label: '崇左',
        value: '80',
        children: []
      },
      { label: '来宾', value: '81', children: [] },
      { label: '梧州', value: '82', children: [] },
      {
        label: '河池',
        value: '83',
        children: []
      },
      { label: '玉林', value: '84', children: [] },
      { label: '贵港', value: '85', children: [] },
      {
        label: '贺州',
        value: '86',
        children: []
      },
      { label: '钦州', value: '87', children: [] },
      { label: '百色', value: '88', children: [] }
    ],
    value: -28
  },
  {
    label: '云南',
    children: [
      { label: '昆明', value: '363', children: [] },
      { label: '玉溪', value: '364', children: [] },
      {
        label: '楚雄',
        value: '365',
        children: []
      },
      { label: '大理', value: '366', children: [] },
      { label: '红河', value: '367', children: [] },
      {
        label: '曲靖',
        value: '368',
        children: []
      },
      { label: '西双版纳', value: '369', children: [] },
      { label: '昭通', value: '370', children: [] },
      {
        label: '保山',
        value: '371',
        children: []
      },
      { label: '德宏', value: '372', children: [] },
      { label: '迪庆', value: '373', children: [] },
      {
        label: '丽江',
        value: '374',
        children: []
      },
      { label: '临沧', value: '375', children: [] },
      { label: '怒江', value: '376', children: [] },
      {
        label: '普洱',
        value: '377',
        children: []
      },
      { label: '文山', value: '378', children: [] }
    ],
    value: -29
  },
  {
    label: '内蒙古',
    children: [
      { label: '呼和浩特', value: '248', children: [] },
      { label: '包头', value: '249', children: [] },
      {
        label: '赤峰',
        value: '250',
        children: []
      },
      { label: '鄂尔多斯', value: '251', children: [] },
      { label: '乌兰察布', value: '252', children: [] },
      {
        label: '乌海',
        value: '253',
        children: []
      },
      { label: '兴安盟', value: '254', children: [] },
      { label: '呼伦贝尔', value: '255', children: [] },
      {
        label: '通辽',
        value: '256',
        children: []
      },
      { label: '阿拉善盟', value: '257', children: [] },
      { label: '巴彦淖尔', value: '258', children: [] },
      {
        label: '锡林郭勒',
        value: '259',
        children: []
      }
    ],
    value: -30
  },
  {
    label: '辽宁',
    children: [
      { label: '沈阳', value: '233', children: [] },
      { label: '大连', value: '234', children: [] },
      {
        label: '鞍山',
        value: '235',
        children: []
      },
      { label: '丹东', value: '236', children: [] },
      { label: '抚顺', value: '237', children: [] },
      {
        label: '锦州',
        value: '238',
        children: []
      },
      { label: '营口', value: '239', children: [] },
      { label: '本溪', value: '240', children: [] },
      {
        label: '朝阳',
        value: '241',
        children: []
      },
      { label: '阜新', value: '242', children: [] },
      { label: '葫芦岛', value: '243', children: [] },
      {
        label: '辽阳',
        value: '244',
        children: []
      },
      { label: '盘锦', value: '245', children: [] },
      { label: '铁岭', value: '246', children: [] }
    ],
    value: -31
  },
  {
    label: '广东',
    children: [
      { label: '广州', value: '53', children: [] },
      { label: '深圳', value: '54', children: [] },
      {
        label: '珠海',
        value: '55',
        children: []
      },
      { label: '东莞', value: '56', children: [] },
      { label: '佛山', value: '57', children: [] },
      {
        label: '惠州',
        value: '58',
        children: []
      },
      { label: '江门', value: '59', children: [] },
      { label: '中山', value: '60', children: [] },
      {
        label: '汕头',
        value: '61',
        children: []
      },
      { label: '湛江', value: '62', children: [] },
      { label: '潮州', value: '63', children: [] },
      {
        label: '河源',
        value: '64',
        children: []
      },
      { label: '揭阳', value: '65', children: [] },
      { label: '茂名', value: '66', children: [] },
      {
        label: '梅州',
        value: '67',
        children: []
      },
      { label: '清远', value: '68', children: [] },
      { label: '韶关', value: '69', children: [] },
      {
        label: '汕尾',
        value: '70',
        children: []
      },
      { label: '阳江', value: '71', children: [] },
      { label: '云浮', value: '72', children: [] },
      {
        label: '肇庆',
        value: '73',
        children: []
      }
    ],
    value: -32
  },
  {
    label: '青海',
    children: [
      { label: '西宁', value: '267', children: [] },
      { label: '黄南', value: '268', children: [] },
      {
        label: '玉树',
        value: '269',
        children: []
      },
      { label: '果洛', value: '270', children: [] },
      { label: '海东', value: '271', children: [] },
      {
        label: '海西',
        value: '272',
        children: []
      },
      { label: '海南', value: '273', children: [] },
      { label: '海北', value: '274', children: [] }
    ],
    value: -33
  },
  {
    label: '北京',
    children: [{ label: '北京', value: '2', children: [] }],
    value: -34
  }
]

export default proCityInfo
