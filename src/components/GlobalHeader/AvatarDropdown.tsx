import React from 'react';
import { Avatar, Menu, Dropdown, Spin } from 'antd';
import { LogoutOutlined, SettingOutlined } from '@ant-design/icons';
import { history, connect, ConnectProps } from 'umi';
import { CurrentUser } from '@/pages/account/accountInfo/data';
import avatar from '../../../public/favicon.png';
import styles from './index.less';

export interface AvatarDropdownProps extends Partial<ConnectProps> {
  currentUser?: CurrentUser;
}

class AvatarDropdown extends React.Component<AvatarDropdownProps> {
  onAvatarDropdown = (event: {
    key: React.Key;
    keyPath: React.Key[];
    item: React.ReactInstance;
    domEvent: React.MouseEvent<HTMLElement>;
  }) => {
    const { key } = event;
    if (key === 'logout') {
      const { dispatch } = this.props;
      if (dispatch) {
        dispatch({
          type: 'login/logout'
        });
      }
    } else {
      history.push(`/${key}`);
    }
  };

  render(): React.ReactNode {
    const {
      currentUser = {
        avatar: '',
        name: ''
      }
    } = this.props;

    const menu = (
      <Menu
        className={styles.menu}
        selectedKeys={[]}
        onClick={this.onAvatarDropdown}
      >
        <Menu.Item key="account">
          <SettingOutlined />
          账户管理
        </Menu.Item>
        <Menu.Divider />
        <Menu.Item key="logout">
          <LogoutOutlined />
          退出登录
        </Menu.Item>
      </Menu>
    );

    return currentUser ? (
      <Dropdown overlay={menu} placement="bottomCenter">
        <span>
          <Avatar size="small" src={avatar} alt="avatar" />
          <span>Admin</span>
        </span>
      </Dropdown>
    ) : (
      <span className={`${styles.action} ${styles.account}`}>
        <Spin
          size="small"
          style={{
            marginLeft: 8,
            marginRight: 8
          }}
        />
      </span>
    );
  }
}

const mapStateToProps = ({ accountInfo }: { accountInfo: CurrentUser }) => ({
  currentUser: accountInfo
});

export default connect(mapStateToProps)(AvatarDropdown);
