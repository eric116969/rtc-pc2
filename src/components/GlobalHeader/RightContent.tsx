import React, { useEffect, useState } from 'react';
import { Menu, Badge } from 'antd';
import { BellOutlined } from '@ant-design/icons';
import { history } from 'umi';
import AvatarDropdown from './AvatarDropdown';
import './index.less';

const RightContent = () => {
  const [selectedKey, setSelectedKey] = useState<string[]>([]);

  const onMenuClick = (event: {
    key: React.Key;
    keyPath: React.Key[];
    item: React.ReactInstance;
    domEvent: React.MouseEvent<HTMLElement>;
  }) => {
    const { key } = event;
    if (key === 'account') {
      return;
    } else if (key === 'notice') {
      history.push(`/account/myNotice`);
    } else {
      history.push(`/${key}`);
    }
  };

  const pathname = window.location.pathname;

  const whichKey = () => {
    const pathnameArr = pathname.split('/');
    setSelectedKey([pathnameArr[1]]);
  };

  useEffect(() => {
    whichKey();
  }, [pathname]);

  return (
    <Menu
      mode="horizontal"
      className="topMenu"
      selectedKeys={selectedKey}
      onClick={onMenuClick}
    >
      <Menu.Item key="clinical">流程管理</Menu.Item>
      <Menu.Item key="patient">病例管理</Menu.Item>
      <Menu.Item key="visit">随访管理</Menu.Item>
      <Menu.Item key="cooperate">协作管理</Menu.Item>
      <Menu.Item key="research">科研管理</Menu.Item>
      <Menu.Item key="notice">
        <Badge dot>
          <BellOutlined />
        </Badge>
      </Menu.Item>
      <Menu.Item key="account">
        <AvatarDropdown />
      </Menu.Item>
    </Menu>
  );
};

export default RightContent;
