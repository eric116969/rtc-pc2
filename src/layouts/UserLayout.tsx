import {
  DefaultFooter,
  MenuDataItem,
  getMenuData,
  getPageTitle
} from '@ant-design/pro-layout';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Link } from 'umi';
import React from 'react';
import logo from '../../public/favicon.png';
import styles from './UserLayout.less';

const UserLayout = (props: any) => {
  const { route = { routes: [] } } = props;
  const { routes = [] } = route;
  const { children, location = { pathname: '' } } = props;
  const { breadcrumb } = getMenuData(routes);
  const title = getPageTitle({
    pathname: location.pathname,
    breadcrumb,
    ...props,
    title: '放疗云'
  });
  console.log(title, breadcrumb, props);

  return (
    <HelmetProvider>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={title} />
      </Helmet>

      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.top}>
            <div className={styles.header}>
              <Link to="/">
                <img alt="logo" className={styles.logo} src={logo} />
                <span className={styles.title}>放疗云</span>
              </Link>
            </div>
            <div className={styles.desc}>放疗云 肿瘤放疗专业云平台</div>
          </div>
          {children}
        </div>
      </div>
    </HelmetProvider>
  );
};

export default UserLayout;
