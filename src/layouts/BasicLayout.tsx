import React from 'react';
import ProLayout, {
  MenuDataItem,
  Settings,
  getPageTitle,
  GridContent,
  PageContainer,
  BasicLayoutProps as ProLayoutProps
} from '@ant-design/pro-layout';
import { Dispatch, Link } from 'umi';
import RightContent from '@/components/GlobalHeader/RightContent';
import hcmedLogo from '../../public/logo_nav.png';
// import menu from 'mock/menu';

export interface BasicLayoutProps extends ProLayoutProps {
  breadcrumbNameMap: {
    [path: string]: MenuDataItem;
  };
  route: ProLayoutProps['route'] & {
    authority: string[];
  };
  settings: Settings;
  dispatch: Dispatch;
}

const menuDataRender = (menuList: MenuDataItem[]): MenuDataItem[] => {
  // console.log('menuList', window.location.href, menuList);
  return menuList.map((item) => {
    const localItem = {
      ...item,
      children: item.children ? menuDataRender(item.children) : undefined
    };
    // console.log(localItem);
    return localItem as MenuDataItem;
    // return Authorized.check(item.authority, localItem, null) as MenuDataItem;
  });
};

const BasicLayout: React.FC<BasicLayoutProps> = (props) => {
  const { children } = props;
  // console.log(props);

  return (
    <>
      <ProLayout
        navTheme="light"
        layout="mix"
        title=""
        logo={hcmedLogo}
        siderWidth={200}
        fixSiderbar={true}
        fixedHeader={true}
        rightContentRender={RightContent}
        menuItemRender={(menuItemProps, defaultDom) => {
          if (menuItemProps.isUrl || !menuItemProps.path) {
            return defaultDom;
          }
          // console.log(menuItemProps)
          return <Link to={menuItemProps.path}>{defaultDom}</Link>;
        }}
        menuDataRender={menuDataRender}
        {...props}
      >
        <PageContainer>{children}</PageContainer>
      </ProLayout>
    </>
  );
};

export default BasicLayout;
