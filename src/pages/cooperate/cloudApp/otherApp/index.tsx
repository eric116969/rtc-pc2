import React, { Component } from 'react';

class OtherApp extends Component<any, any> {
  constructor(props: Readonly<{}>) {
    console.log('01构造函数');
    super(props);
    this.state = {};
  }
  //是否要更新数据，如果返回true才会更新数据
  shouldComponentUpdate(nextProps: any, nextState: any) {
    console.log('01是否要更新数据');
    console.log(nextProps); //父组件传给子组件的值，这里没有会显示空
    console.log(nextState); //数据更新后的值
    return true; //返回true，确认更新
  }
  //组件将要挂载时候触发的生命周期函数
  componentWillMount() {
    console.log('02组件将要挂载');
  }
  //组件挂载完成时候触发的生命周期函数
  componentDidMount() {
    console.log('04组件将要挂载');
  }
  render() {
    console.log('03数据渲染render');
    return <div>OtherApp</div>;
  }
}
export default OtherApp;
