import { Reducer, Effect, Subscription } from 'umi';
import { getEnJournalArr } from './service';
import { EnJournalArr } from './data.j';

export interface ModalState {
  enJournalArr?: Partial<EnJournalArr>;
}

export interface ModelType {
  namespace: string;
  state: ModalState;
  effects: {
    getEnJournalArr: Effect;
  };
  reducers: {
    saveCurrentUser: Reducer;
  };
  subscriptions: {
    init: Subscription;
  };
}

const Model: ModelType = {
  namespace: 'journal',

  state: {
    enJournalArr: {}
  },

  effects: {
    *getEnJournalArr(_, { call, get }) {
      const response = yield call(getEnJournalArr);
      console.log('get current user info', response);
      yield get({
        type: 'getEnJournalArr',
        payload: response
      });
    }
  },

  reducers: {
    saveCurrentUser(state, action) {
      return {
        ...state,
        CurrentUser: action.payload || {}
      };
    }
  },

  subscriptions: {
    init({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/account/accountInfo') {
          dispatch({
            type: 'getCurrentUser'
          });
        }
      });
    }
  }
};

export default Model;
