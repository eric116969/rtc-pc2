import request from '@/utils/request';

export async function getEnJournalArr() {
  return request('/research/journals');
}
