export interface EnJournalArr {
  url: string;
  abbrTitle: string;
  coverPath: string;
}
