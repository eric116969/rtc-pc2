import React, { Component } from 'react';
import { Tabs, Row, Col } from 'antd';
import './style.less';
const { TabPane } = Tabs;

function callback(key: any) {
  console.log(key);
}

class Journal extends Component<any, any> {
  constructor(props: Readonly<{}>) {
    console.log('01构造函数');
    super(props);
    this.state = {};
  }

  //组件将要挂载时候触发的生命周期函数
  componentWillMount() {
    console.log('02组件将要挂载');
  }
  //组件挂载完成时候触发的生命周期函数
  componentDidMount() {
    console.log('04组件将要挂载');
  }
  render() {
    const zhJournalArr = [
      {
        abbrTitle: '癌症进展',
        coverPath: '/images/research/azjz.png',
        url: 'http://aizhengjizhan.yywkt.com/'
      },
      {
        abbrTitle: '癌症杂志',
        coverPath: '/images/research/azzz.png',
        url: 'http://www.china-oncology.com/'
      },
      {
        abbrTitle: '临床肿瘤学杂志',
        coverPath: '/images/research/lczlzz.png',
        url: 'http://manu65.magtech.com.cn/Jwk3_lczlxzz/CN/1009-0460/home.shtml'
      },
      {
        abbrTitle: '中国医学物理学杂志',
        coverPath: '/images/research/zgyxwlxzz.png',
        url: 'http://zgyxwlxzz.paperopen.com/'
      },
      {
        abbrTitle: '中国肿瘤',
        coverPath: '/images/research/zgzl.png',
        url: 'http://www.chinaoncology.cn/'
      },
      {
        abbrTitle: '中国肿瘤临床',
        coverPath: '/images/research/zgzllc.png',
        url:
          'http://journal11.magtechjournal.com/Jwk_zgzllc/CN/volumn/home.shtml'
      },
      {
        abbrTitle: '中华放射医学与防护杂志',
        coverPath: '/images/research/zhfsyxyfhzz.png',
        url: 'http://www.cjrmp.net/'
      },
      {
        abbrTitle: '中华放射肿瘤学杂志',
        coverPath: '/images/research/zhfszlxzz.png',
        url:
          'http://journal12.magtechjournal.com/Jweb_fszlx/CN/volumn/home.shtml'
      },
      {
        abbrTitle: '中华医学杂志',
        coverPath: '/images/research/zhyxzz.png',
        url: 'http://zhyxzz.yiigle.com/'
      },
      {
        abbrTitle: '中华肿瘤防治杂志',
        coverPath: '/images/research/zhzlfzzz.png',
        url: 'http://www.cjcpt.org/index.php/zh/topmenu-home-zh-cn'
      },
      {
        abbrTitle: '中华肿瘤杂志',
        coverPath: '/images/research/zhzlzz.png',
        url: 'http://www.chinjoncol.com/'
      },
      {
        abbrTitle: '肿瘤学杂志',
        coverPath: '/images/research/zlxzz.png',
        url: 'http://www.chinaoncology.cn/zlxzz8/ch/index.aspx'
      }
    ];
    function ListItems(props: any) {
      const zhJournalArr = props.zhJournalArr;
      function jumpTo(e: any, item: any) {
        console.log(item);
        window.open(item.url);
      }
      const listItems = zhJournalArr.map((item: any, i: number) => (
        <Col
          span={6}
          key={i}
          className={[
            'text-center',
            { mdClear: i % 4 === 0, smClear: i % 3 === 0 }
          ].join(' ')}
        >
          <img
            className="bookImg"
            onClick={(e) => {
              jumpTo(e, item);
            }}
            src={item.coverPath}
          />
          <p>{item.abbrTitle}</p>
        </Col>
      ));
      return <Row>{listItems}</Row>;
    }
    const Demo = () => (
      <Tabs defaultActiveKey="English" onChange={callback}>
        <TabPane tab="英文期刊" key="English">
          英文期刊
        </TabPane>
        <TabPane tab="中文期刊" key="Chinese">
          <ListItems className="row" zhJournalArr={zhJournalArr} />
        </TabPane>
      </Tabs>
    );
    console.log('03数据渲染render');
    return (
      <div>
        <Demo />
      </div>
    );
  }
}
export default Journal;
