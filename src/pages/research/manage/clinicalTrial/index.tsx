import React, { Component } from 'react';
import './../../style.less';
import { Typography } from 'antd';

const { Title } = Typography;

class ClinicalTrial extends Component<any, any> {
  constructor(props: Readonly<{}>) {
    console.log('01构造函数');
    super(props);
    this.state = {};
  }
  //组件将要挂载时候触发的生命周期函数
  componentWillMount() {
    console.log('02组件将要挂载');
  }
  //组件挂载完成时候触发的生命周期函数
  componentDidMount() {
    console.log('04组件将要挂载');
  }
  render() {
    console.log('03数据渲染render');
    return (
      <div className="wrapContent">
        <div className="content">
          <Title className="text-center noModule">
            <i className="fa fa-medkit"></i>该模块暂未开通
          </Title>
        </div>
      </div>
    );
  }
}
export default ClinicalTrial;
