import React, { Component } from 'react';
import request from '@/utils/request';
// 指南规范

class GuideSet extends Component<any, any> {
  constructor(props: Readonly<{}>) {
    console.log('01构造函数');
    super(props);
    this.state = {
      msg: '我是一个msg数据',
      enJournalArr: [1, 2, 3]
    };
  }
  //是否要更新数据，如果返回true才会更新数据
  shouldComponentUpdate(nextProps: any, nextState: any) {
    console.log('01是否要更新数据');
    console.log(nextProps); //父组件传给子组件的值，这里没有会显示空
    console.log(nextState); //数据更新后的值
    return true; //返回true，确认更新
  }
  //组件将要挂载时候触发的生命周期函数
  componentWillMount() {
    this.setState({
      enJournalArr: [6, 6, 6]
    });
    function getEnJournalArr() {
      return request('/research/journals');
    }
    const arr = getEnJournalArr();
    console.log(arr);
    console.log('02组件将要挂载');
  }
  //组件挂载完成时候触发的生命周期函数
  componentDidMount() {
    console.log('04组件将要挂载');
  }
  setMsg() {
    this.setState({
      msg: '我是改变后的msg数据'
    });
  }
  render() {
    console.log('03数据渲染render');
    console.log(this.state.enJournalArr);
    return (
      <div>
        {this.state.msg}
        <br />
        <hr />
        <button onClick={() => this.setMsg()}>更新msg的数据</button>
      </div>
    );
  }
}
export default GuideSet;
