import React from 'react';
import './../style.less';
import { Typography } from 'antd';

const { Title } = Typography;
const Serve = () => {
  return (
    <div className="wrapContent">
      <div className="content">
        <Title className="text-center noModule">
          <i className="fa fa-medkit"></i>该模块暂未开通
        </Title>
      </div>
    </div>
  );
};

export default Serve;
