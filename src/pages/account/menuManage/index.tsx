import React, { useState, FC } from 'react';
import { Button, Popconfirm, Pagination, message } from 'antd';
import ProTable, { ProColumns } from '@ant-design/pro-table';
import { PlusOutlined } from '@ant-design/icons';
import MenuModal from './components/MenuModal';
import { MenuItemType, FormValues } from './data';
import { connect, Dispatch, Loading } from 'umi';

const MenuManage = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [record, setRecord] = useState(undefined);

  const columns: ProColumns<MenuItemType>[] = [
    {
      title: 'ID',
      dataIndex: 'id',
      valueType: 'digit',
      key: 'id'
    },
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
      valueType: 'text',
      render: (text: any) => <a>{text}</a>
    },
    {
      title: '图标',
      dataIndex: 'icon',
      valueType: 'text',
      key: 'icon'
    },
    {
      title: '路径',
      dataIndex: 'path',
      valueType: 'text',
      key: 'path'
    },
    {
      title: '操作',
      key: 'action',
      valueType: 'option',
      render: (text: any, record: MenuItemType) => [
        <a
          onClick={() => {
            editHandler(record);
          }}
        >
          Edit
        </a>,
        <Popconfirm
          title="Are you sure delete this task?"
          onConfirm={() => {
            confirm(record);
          }}
          okText="Yes"
          cancelText="No"
        >
          <a>Delete</a>
        </Popconfirm>
      ]
    }
  ];

  const addHandler = () => {
    setModalVisible(true);
  };

  const closeHandler = () => {
    setModalVisible(false);
  };

  const onFinish = () => {
    setConfirmLoading(true);
  };

  return (
    <div className="list-table">
      <ProTable
        columns={columns}
        rowKey="id"
        headerTitle="菜单列表"
        toolBarRender={() => [
          <Button key="3" type="primary" onClick={addHandler}>
            <PlusOutlined />
            新建
          </Button>
        ]}
      ></ProTable>
      <MenuModal
        visible={modalVisible}
        closeHandler={closeHandler}
        record={record}
        onFinish={onFinish}
        confirmLoading={confirmLoading}
      />
    </div>
  );
};

export default MenuManage;
