import React from 'react';
import { Modal, Button, Form, Input, DatePicker, Switch, Select } from 'antd';
import { createFromIconfontCN } from '@ant-design/icons';
import { MenuItemType, FormValues } from '../data';
import moment from 'moment';

const { Option } = Select;

const layout = {
  labelCol: { span: 4 },
  wrapperCol: { span: 20 }
};

const MenuModal = (props) => {
  const [form] = Form.useForm();
  const { visible, record, closeHandler, onFinish, confirmLoading } = props;

  const onOk = () => {
    form.submit();
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <Modal
        title={record ? 'Edit ID: ' + record.id : 'Add'}
        visible={visible}
        onOk={onOk}
        onCancel={closeHandler}
        okText="确认"
        cancelText="取消"
        forceRender
        confirmLoading={confirmLoading}
      >
        <Form
          {...layout}
          name="basic"
          form={form}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          initialValues={{
            status: true
          }}
        >
          <Form.Item
            label="菜单名称"
            name="name"
            rules={[{ required: true, message: '请输入菜单名称!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="所属模块"
            name="root"
            rules={[{ required: true, message: '请选择所属模块!' }]}
          >
            <Select placeholder="请选择所属模块">
              <Option value="clinical">流程管理</Option>
              <Option value="patient">病例管理</Option>
              <Option value="visit">随访管理</Option>
              <Option value="cooperate">协作管理</Option>
              <Option value="research">科研管理</Option>
              <Option value="account">账户管理</Option>
            </Select>
          </Form.Item>
          <Form.Item
            label="组件路径"
            name="path"
            rules={[{ required: true, message: '请输入组件路径!' }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="状态" name="status" valuePropName="checked">
            <Switch checkedChildren="开启" unCheckedChildren="关闭" />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};

export default MenuModal;
