export interface MenuItemType {
  id: number;
  root: string;
  name: string;
  icon: string;
  path: string;
  status: number;
}

export interface FormValues {
  [name: string]: any;
}
