import { Reducer, Effect, Subscription } from 'umi';
import { CurrentUser } from './data.d';
import { queryCurrent } from './service';

export interface ModalState {
  currentUser?: Partial<CurrentUser>;
  isLoading?: boolean;
}

export interface ModelType {
  namespace: string;
  state: ModalState;
  effects: {
    getCurrentUser: Effect;
  };
  reducers: {
    saveCurrentUser: Reducer;
  };
  subscriptions: {
    init: Subscription;
  };
}

const Model: ModelType = {
  namespace: 'accountInfo',

  state: {
    currentUser: {},
    isLoading: false
  },

  effects: {
    *getCurrentUser(_, { call, put }) {
      const response = yield call(queryCurrent);
      console.log('get current user info', response);
      yield put({
        type: 'saveCurrentUser',
        payload: response
      });
    }
  },

  reducers: {
    saveCurrentUser(state, action) {
      return {
        ...state,
        CurrentUser: action.payload || {}
      };
    }
  },

  subscriptions: {
    init({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname === '/account/accountInfo') {
          dispatch({
            type: 'getCurrentUser'
          });
        }
      });
    }
  }
};

export default Model;
