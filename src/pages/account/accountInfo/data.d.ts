export interface CurrentUser {
  id: number;
  name: string;
  username: string;
  password: string;
  gender: string;
  idNumber: string;
  age: string;
  email: string;
  role: string;
  hospital: {
    name: string;
    id: number;
  };
  workNumber: string;
  workGroups: [];
  proTitle: string;
  education: string;
  jobCertificated: boolean;
  staffId: number;
  accountActive: string;
  pageAfterLogin: string;
}
