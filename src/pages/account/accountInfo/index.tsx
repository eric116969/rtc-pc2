import React, { FunctionComponent, useEffect } from 'react';
import { Upload, message, Button, Form, Input, Select, Radio } from 'antd';
import { UploadOutlined } from '@ant-design/icons';
import { Dispatch, connect, Loading } from 'umi';

import { CurrentUser } from './data';

import './style.less';

const { Option } = Select;

const AvatarView = ({ avatar }: { avatar: string }) => (
  <>
    <div className="avatarTitle">头像</div>
    <div className="avatar">
      <img src={avatar} alt="avatar" />
    </div>
    <Upload>
      <div className="buttonView">
        <Button icon={<UploadOutlined />}>上传头像</Button>
      </div>
    </Upload>
  </>
);

interface UserProps {
  dispatch: Dispatch;
  currentUser?: CurrentUser;
}

const formItemLayout = {
  labelCol: { span: 6 },
  wrapperCol: { span: 18 }
};

const onFinish = (values: any) => {
  console.log('Form values: ', values);
};

const AccountInfo: FunctionComponent<UserProps> = (props) => {
  const { dispatch } = props;

  // useEffect(() => {
  //   console.log('useeffect');
  //   dispatch({
  //     type: 'accountInfo/getCurrentUser',
  //     payload: {}
  //   });
  // }, []);

  return (
    <div className="mainContent">
      <div className="left">
        <Form {...formItemLayout} onFinish={onFinish}>
          <Form.Item
            name="name"
            label="姓名"
            rules={[
              {
                required: true,
                message: '请输入姓名'
              }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="username"
            label="用户名"
            rules={[
              {
                required: true,
                message: '请输入用户名'
              }
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item name="password" label="密码">
            <Input />
          </Form.Item>
          <Form.Item name="gender" label="性别">
            <Input />
          </Form.Item>
          <Form.Item name="idNumber" label="身份证">
            <Input />
          </Form.Item>
          <Form.Item name="age" label="年龄">
            <Input />
          </Form.Item>
          <Form.Item name="email" label="邮箱">
            <Input />
          </Form.Item>
          <Form.Item name="hospital" label="医院">
            <Input />
          </Form.Item>
          <Form.Item label="科室">
            <span>放疗科</span>
          </Form.Item>
          <Form.Item
            name="role"
            label="角色"
            rules={[
              {
                required: true,
                message: '请选择角色'
              }
            ]}
          >
            <Select allowClear>
              <Option value="ROLE_DOCTOR">医生</Option>
              <Option value="ROLE_PHYSICIST">物理师</Option>
              <Option value="ROLE_ENGINEER">工程师</Option>
              <Option value="ROLE_TECHNICIAN">技师</Option>
              <Option value="ROLE_NURSE">护士</Option>
            </Select>
          </Form.Item>
          <Form.Item name="workGroups" label="工作组">
            <Select />
          </Form.Item>
          <Form.Item name="workNumber" label="工号">
            <Input />
          </Form.Item>
          <Form.Item name="proTitle" label="职称">
            <Select allowClear>
              <Option value="初级">初级</Option>
              <Option value="中级">中级</Option>
              <Option value="高级">高级</Option>
            </Select>
          </Form.Item>
          <Form.Item name="education" label="学历">
            <Select allowClear>
              <Option value="专科">专科</Option>
              <Option value="本科">本科</Option>
              <Option value="硕士">硕士</Option>
              <Option value="博士">博士</Option>
            </Select>
          </Form.Item>
          <Form.Item name="jobCertificated" label="上岗证">
            <Radio.Group>
              <Radio value={true}>有</Radio>
              <Radio value={false}>无</Radio>
            </Radio.Group>
          </Form.Item>
          <Form.Item name="staffId" label="关联账户">
            <Select />
          </Form.Item>
          <Form.Item wrapperCol={{ span: 12, offset: 6 }}>
            <Button type="primary" htmlType="submit">
              更新信息
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className="right">
        <AvatarView avatar={''} />
      </div>
    </div>
  );
};

const mapStateToProps = ({
  currentUser,
  isLoading
}: {
  currentUser: CurrentUser;
  isLoading: Loading;
}) => {
  return {
    currentUser,
    loading: isLoading
  };
};

export default connect(mapStateToProps)(AccountInfo);
