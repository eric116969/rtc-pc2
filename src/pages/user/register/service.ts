import request from '@/utils/request';
import { RegisterParams } from './index';

export async function register(params: RegisterParams) {
  return request('', {
    method: 'POST',
    data: params
  });
}

export async function captcha(mobile: string) {
  return request(`/api/login/captcha?mobile=${mobile}`);
}
