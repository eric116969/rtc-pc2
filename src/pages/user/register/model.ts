import { Effect, Reducer } from 'umi';
import { register, captcha } from './service';

export interface StateType {
  status?: 'ok' | 'error';
  currentAuthority?: 'user' | 'guest' | 'admin';
}

export interface ModelType {
  namespace: string;
  state: StateType;
  effects: {
    submit: Effect;
    getCaptcha: Effect;
  };
  reducers: {};
}

const Model: ModelType = {
  namespace: 'userRegister',
  state: {
    status: undefined
  },

  effects: {
    *submit({ payload }, { call, put }) {
      console.log('payload', payload);
      const response = yield call(register, payload);
    },
    *getCaptcha({ payload }, { call, put }) {
      yield call(captcha, payload);
    }
  },
  reducers: {}
};
