import React, { useState, FC, useCallback } from 'react';
import { Form, Input, Cascader, Select, Button, Tabs, Row, Col } from 'antd';
import {
  MedicineBoxOutlined,
  TeamOutlined,
  UserOutlined,
  NumberOutlined,
  MobileOutlined
} from '@ant-design/icons';
import { Link, Dispatch, connect } from 'umi';
import proCityInfo from '@/assets/proCityInfo';
import { StateType } from './model';
import './style.less';
import { captcha } from './service';

interface RegisterProps {
  dispatch: Dispatch<any>;
  userRegister: StateType;
  submitting: boolean;
}

export interface RegisterParams {
  residence: object;
  dept: string;
  role: string;
  nickname: string;
  mobile: string;
  verifyCode: string;
}

const { Option } = Select;
const { TabPane } = Tabs;

const roleOptions = [
  {
    value: 'ROLE_DOCTOR',
    label: '医生'
  },
  {
    value: 'ROLE_PHYSICIST',
    label: '物理师'
  },
  {
    value: 'ROLE_ENGINEER',
    label: '工程师'
  },
  {
    value: 'ROLE_TECHNICIAN',
    label: '技师'
  },
  {
    value: 'ROLE_NURSE',
    label: '护士'
  }
];

const RegistrationForm: FC<RegisterProps> = ({
  submitting,
  dispatch,
  userRegister
}) => {
  const [form] = Form.useForm();

  const [count, setCount]: [number, any] = useState(0);

  let interval: number | undefined;

  const onFinish = (values: { [key: string]: any }) => {
    console.log('Received values of form: ', values);
    dispatch({
      type: 'userRegister/submit',
      payload: {
        ...values
      }
    });
  };

  const onGetCaptcha = useCallback(async (mobile: string) => {
    console.log(mobile);
    const result = await captcha(mobile);
    let counts = 59;
    setCount(counts);
    interval = window.setInterval(() => {
      counts -= 1;
      setCount(counts);
      if (counts === 0) {
        clearInterval(interval);
      }
    }, 1000);
    // dispatch({
    //   type: 'userRegister/getCaptcha',
    //   payload: {}
    // });
  }, []);

  return (
    <div className="formContainer">
      <Tabs>
        <TabPane tab="注册账户">
          <Form
            form={form}
            initialValues={{
              dept: '放疗科'
            }}
            className="registerForm"
            name="register"
            onFinish={onFinish}
            scrollToFirstError
          >
            <Form.Item>
              <MedicineBoxOutlined className="registerSpecialLogo" />
              <Form.Item
                name="residence"
                noStyle
                rules={[
                  {
                    type: 'array',
                    required: true,
                    message: '请选择你所在的医院!'
                  }
                ]}
              >
                <Cascader
                  options={proCityInfo}
                  placeholder="请选择你所在的医院！"
                />
              </Form.Item>
            </Form.Item>

            <Form.Item name="dept">
              <Input
                disabled
                prefix={<MedicineBoxOutlined className="registerLogo" />}
              />
            </Form.Item>

            <Form.Item>
              <TeamOutlined className="registerSpecialLogo" />
              <Form.Item
                name="role"
                noStyle
                rules={[
                  {
                    required: true,
                    message: '请选择你的角色!'
                  }
                ]}
              >
                <Select
                  allowClear={true}
                  options={roleOptions}
                  placeholder="请选择你的角色!"
                ></Select>
              </Form.Item>
            </Form.Item>

            <Form.Item
              name="nickname"
              rules={[
                {
                  required: true,
                  message: '请输入你的用户名!',
                  whitespace: true
                }
              ]}
            >
              <Input
                prefix={<UserOutlined className="registerLogo" />}
                placeholder="用户名"
              />
            </Form.Item>

            <Form.Item shouldUpdate>
              {({ getFieldValue }) => (
                <Row gutter={8}>
                  <Col span={16}>
                    <Form.Item
                      name="mobile"
                      rules={[
                        {
                          required: true,
                          message: '请输入你的手机号!'
                        }
                      ]}
                      noStyle
                    >
                      <Input
                        prefix={<MobileOutlined className="registerLogo" />}
                        placeholder="手机号"
                      />
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Button
                      type="primary"
                      disabled={!!count}
                      className="captchaButton"
                      onClick={() => {
                        const value = getFieldValue('mobile');
                        onGetCaptcha(value);
                      }}
                    >
                      {count ? `${count} s` : '获取验证码'}
                    </Button>
                  </Col>
                </Row>
              )}
            </Form.Item>

            <Form.Item
              name="verifyCode"
              rules={[
                {
                  required: true,
                  message: '请输入你的验证码!'
                }
              ]}
            >
              <Input
                prefix={<NumberOutlined className="registerLogo" />}
                placeholder="验证码"
              />
            </Form.Item>

            <Form.Item>
              <Button
                loading={submitting}
                type="primary"
                htmlType="submit"
                className="registerFormButton"
              >
                注 册
              </Button>
            </Form.Item>
          </Form>
          <div className="other">
            <Link className="toLogin" to="/user/login">
              立即登录
            </Link>
          </div>
        </TabPane>
      </Tabs>
    </div>
  );
};

export default connect(
  ({
    userRegister,
    loading
  }: {
    userRegister: StateType;
    loading: {
      effects: {
        [key: string]: boolean;
      };
    };
  }) => ({
    userRegister,
    submitting: loading.effects['userRegister/submit']
  })
)(RegistrationForm);
