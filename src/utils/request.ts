import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import qs from 'qs';
import Cookie from 'js-cookie';
import { history } from 'umi';
import { LoginParamsType } from '@/pages/user/login/service';

const isLocalize = true;

const baseUrl = {
  baseURL: 'http://192.168.0.109:7001'
};

const getAxiosInstance = (): AxiosInstance => {
  const instance: AxiosInstance = axios.create(baseUrl);
  // 请求拦截器
  instance.interceptors.request.use(
    (config: AxiosRequestConfig) => {
      if (window.location.origin.includes('3f17279353.oicp.vip')) {
        config.url = 'http://3g177935f3.qicp.vip:27057' + config.url;
      }
      if (config.url && config.url.indexOf('/oauth/token') > -1) {
        config.headers.Authorization = 'Basic cnRjOnJ0Y3NlY3JldA==';
      } else {
        const url = config.url;
        if (url && url.indexOf('/autoReview/') > -1) {
          if (Cookie.get('user_token'))
            config.headers.user_token = Cookie.get('user_token');
        } else if (url && url.indexOf('/raicois') > -1) {
        } else if (Cookie.get('token')) {
          config.headers.Authorization = 'bearer ' + Cookie.get('token');
        } else if (config.params && config.params.__cookie) {
          config.headers.Authorization = 'bearer ' + config.params.__cookie;
        }
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
  // 响应拦截器
  instance.interceptors.response.use(
    (response) => {
      if (response && response.data) {
        return Promise.resolve(response);
      } else {
        return Promise.reject('response 不存在');
      }
    },
    (error) => {
      if (
        error.response.status === 401 &&
        (error.response.data.error === 'invalid token' ||
          error.response.data.error === 'unauthorized')
      ) {
        history.replace({
          pathname: '/user/login'
        });
      } else {
        return Promise.resolve({
          data: {
            success: false,
            msg: typeof error === 'string' ? error : error.message
          }
        });
      }
    }
  );
  return instance;
};

// 对比上一次请求是否超过30分钟
let prevTime: number;
const compareTime = () => {
  let sessionTime: number;
  const ajaxTime = localStorage.getItem('ajaxTime');
  if (ajaxTime && ajaxTime !== null) {
    sessionTime = +ajaxTime;
  } else {
    sessionTime = 0;
  }
  if (sessionTime) prevTime = sessionTime;
  if (!prevTime) prevTime = new Date().getTime();
  const difference = new Date().getTime() - prevTime;
  const time = isLocalize ? 86400000 : 1800000;
  if (difference >= time && window.location.pathname !== '/') {
    history.replace({
      pathname: '/user/login'
    });
  }
  prevTime = new Date().getTime();
  localStorage.setItem('ajaxTime', String(prevTime));
};

const instance: AxiosInstance = getAxiosInstance();

const request = {
  login(url: string, params: LoginParamsType) {
    prevTime = new Date().getTime();
    localStorage.setItem('ajaxTime', String(prevTime));
    const query = qs.stringify(params);
    return instance(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      data: query
    });
  },
  post(url: string, query: any, params = {}) {
    compareTime();
    query = qs.stringify(query);
    return instance(url, {
      method: 'POST',
      data: query,
      params
    });
  },
  postJson(url: string, query: any, params = {}) {
    compareTime();
    const headers: any = { 'Content-Type': 'application/json' };
    if (query && query._jwt) headers.Authorization = query._jwt;
    return instance(url, {
      method: 'POST',
      data: query,
      headers,
      params
    });
  },
  postExportExcel(url: string, query: any, params = {}) {
    compareTime();
    const headers = { 'Content-Type': 'application/json' };
    return instance(url, {
      method: 'POST',
      data: query,
      headers,
      responseType: 'blob',
      params
    });
  },
  put(url: string, query: any, params = {}) {
    compareTime();
    const headers: any = {};
    if (query && query._jwt) headers.Authorization = query._jwt;
    query = qs.stringify(query);
    return instance(url, {
      method: 'PUT',
      data: query,
      headers,
      params
    });
  },
  putJson(url: string, query: any, params = {}) {
    compareTime();
    const headers = { 'Content-Type': 'application/json' };
    return instance(url, {
      method: 'PUT',
      data: query,
      headers,
      params
    });
  },
  get(url: string, query?: any) {
    compareTime();
    const option = { params: query };
    // option.cancelToken = source.token// 这句很重要
    return instance(url, {
      method: 'GET',
      params: query
    });
  },
  getImage(url: string, query: any) {
    compareTime();
    return instance(url, {
      method: 'GET',
      headers: { 'Content-Type': 'image/jpg' },
      responseType: 'arraybuffer',
      params: query
    });
  },
  delete(url: string, query: any) {
    compareTime();
    return instance(url, {
      method: 'DELETE',
      params: query
    });
  },
  deleteJson(url: string, query: any) {
    compareTime();
    return instance(url, {
      method: 'DELETE',
      data: query
    });
  }
};

export const Axios: AxiosInstance = instance;

export default request;
