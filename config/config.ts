import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none'
  },
  title: '放疗云',
  dva: {
    hmr: true
  },
  theme: {
    'primary-color': '#00af86'
  },
  routes: [
    {
      path: '/',
      component: '../layouts/BlankLayout',
      routes: [
        // login
        {
          path: '/user',
          component: '../layouts/UserLayout',
          routes: [
            { path: '/user', redirect: '/user/login' },
            {
              name: '登录',
              path: '/user/login',
              component: './user/login'
            },
            {
              name: '注册',
              path: '/user/register',
              component: './user/register'
            }
          ]
        },
        {
          path: '/',
          component: '../layouts/BasicLayout',
          authority: ['admin', 'user'],
          routes: [
            // after login
            {
              path: '/',
              redirect: '/clinical/recoreSheet'
            },
            // clinical
            {
              path: '/clinical',
              name: '流程管理',
              icon: '',
              routes: [
                {
                  path: '/clinical',
                  redirect: '/clinical/recordSheet'
                },
                {
                  name: '定位申请',
                  icon: '',
                  path: '/clinical/recordSheet',
                  component: './clinical/recordSheet'
                }
              ]
            },
            // account
            {
              path: '/account',
              name: '账户管理',
              icon: '',
              routes: [
                {
                  path: '/account',
                  redirect: '/account/accountInfo'
                },
                {
                  name: '账户信息',
                  icon: '',
                  path: '/account/accountInfo',
                  component: './account/accountInfo'
                },
                {
                  name: '修改密码',
                  icon: '',
                  path: '/account/changePassword',
                  component: './account/changePassword'
                },
                {
                  name: '我的提醒',
                  icon: '',
                  path: '/account/myNotice',
                  component: './account/myNotice'
                },
                {
                  name: '角色管理',
                  icon: '',
                  path: '/account/roleManage',
                  component: './account/roleManage'
                },
                {
                  name: '菜单管理',
                  icon: '',
                  path: '/account/menuManage',
                  component: './account/menuManage'
                }
              ]
            },
            // research
            {
              path: '/research',
              name: '科研管理',
              icon: '',
              routes: [
                {
                  path: '/research',
                  redirect: '/research/manage/journal'
                },
                {
                  name: '科研资料',
                  icon: '',
                  path: '/research/manage',
                  routes: [
                    {
                      name: '期刊文献',
                      icon: '',
                      path: '/research/manage/journal',
                      component: './research/manage/journal'
                    },
                    {
                      name: '指南规范',
                      icon: '',
                      path: '/research/manage/guideSet',
                      component: './research/manage/guideSet'
                    },
                    {
                      name: '临床试验',
                      icon: '',
                      path: '/research/manage/clinicalTrial',
                      component: './research/manage/clinicalTrial'
                    },
                    {
                      name: '基金申请',
                      icon: '',
                      path: '/research/manage/fundApply',
                      component: './research/manage/fundApply'
                    }
                  ]
                },
                {
                  name: '科研助手',
                  icon: '',
                  path: '/research/assistant',
                  component: './research/assistant'
                },
                {
                  name: '科研服务',
                  icon: '',
                  path: '/research/serve',
                  component: './research/serve'
                },
                {
                  name: '智能科研',
                  icon: '',
                  path: '/research/intelligent',
                  component: './research/intelligent'
                }
              ]
            },
            // questionnaire
            {
              path: '/questionnaire',
              name: '随访管理',
              icon: '',
              routes: [
                {
                  path: '/questionnaire',
                  redirect: '/questionnaire/followDb'
                },
                {
                  name: '随访库',
                  icon: '',
                  path: '/questionnaire/followDb',
                  component: './questionnaire/followDb'
                }
              ]
            },
            // research
            {
              path: '/cooperate',
              name: '协作管理',
              icon: '',
              routes: [
                {
                  path: '/cooperate',
                  redirect: '/cooperate/cloudApp/nativeApp'
                },
                {
                  name: '云应用',
                  icon: '',
                  path: '/cooperate/cloudApp',
                  routes: [
                    {
                      name: '原生应用',
                      icon: '',
                      path: '/cooperate/cloudApp/nativeApp',
                      component: './cooperate/cloudApp/nativeApp'
                    },
                    {
                      name: '第三方应用',
                      icon: '',
                      path: '/cooperate/cloudApp/otherApp',
                      component: './cooperate/cloudApp/otherApp'
                    }
                  ]
                },
                {
                  name: '云桌面',
                  icon: '',
                  path: '/cooperate/cloudDesk',
                  component: './cooperate/cloudDesk'
                },
                {
                  name: '协作任务',
                  icon: '',
                  path: '/cooperate/cooperateTask',
                  component: './cooperate/cooperateTask'
                }
              ]
            }
          ]
        }
      ]
    }
  ]
});
